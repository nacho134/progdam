1. Ejercicio FECHAS-HORAS

Para resolver el siguiente ejercicio utiliza la documentación de la API de Java.

Realiza un programa que se ejecute de la forma:
    
    java programa dia mes año Unidad

y, haciendo uso de las clases LocalDate, LocalTime, LocalDateTime, etc. muestre el tiempo transcurrido entre la fecha indicada en los parámetros de ejecución y el instante de la ejecución en la unidad indicada (Segundos, Días o Años). El programa habrá de comprobar que la fecha indicada no pertenezca al futuro.

Ejemplos de ejecución:

    java programa 26 5 2050 Segundos

   , generará la salida

        Fecha futura (no válida)

     java programa 13 8 1975 Años

    , generará la salida

        Han transcurrido xx años

2. Ejercicio WRAPPERS

Realiza un programa en Java que haga lo siguiente:

- lea un valor numérico desde teclado
- cree un objeto Double para el valor introducido con el constructor que requiere como parámetro un String
- obtenga el valor double (tipo básico) correspondiente
- cree un segundo objeto Double para el valor introducido con el constructor que requiere como parámetro el double obtenido anteriormente
- obtenga a partir del Double primero el valor entero (int) más próximo (redondeo) y el valor truncado
- obtenga el valor hexadecimal, octal y binario correspondiente al entero truncado.

EJERCICIOS HERENCIA

3. Codifica la jerarquía de clases Java representada por el diagrama UML adjunto teniendo en cuenta que:

 - La clase base es la clase Empleado. Esta clase contiene:
 - Un atributo protegido nombre de tipo String que heredan el resto de clases. 
 - Un constructor por defecto.
 - Un constructor con parámetros que inicializa el nombre con el String que recibe.
 - Método set y get para el atributo nombre.
 - Un método toString() que devuelve el String: "Empleado " + nombre.

El resto de clases solo deben sobrescribir el método toString() en cada una de ellas y declarar el constructor adecuado de forma que cuando se ejecuten las siguientes instrucciones:

	Empleado E1 = new Empleado("Rafa");
	Directivo D1 = new Directivo("Mario");
	Operario OP1 = new Operario("Alfonso");
	Oficial OF1 = new Oficial("Luis");
	Tecnico T1 = new Tecnico("Pablo");
	System.out.println(E1);
	System.out.println(D1);
	System.out.println(OP1);
	System.out.println(OF1);
	System.out.println(T1);

den como resultado:

	Empleado Rafa
	Empleado Mario -> Directivo
	Empleado Alfonso -> Operario
	Empleado Luis -> Operario -> Oficial
	Empleado Pablo -> Operario -> Tecnico

4. a) Define una clase Vehiculo (ruedas, velocidad y velocidad máxima) que permita detener, acelerar y mover cada vehículo.

Define dos clases derivadas, Bicicleta (marchas) y Motorizado (potencia en CV, caballos de vapor, de tipo double).

Define, a su vez, dos clases derivadas de Motorizado: Motocicleta (tipo) y Automóvil (puertas).

Instancia las 4 clases poniendo a prueba todos sus métodos.

b) Modifica la clase Vehiculo para hacerla abstracta. Incluye en Motorizado un método abstracto getPotenciaReal. La potencia real para motocicletas se expresará en vatios, y para automóviles en kilovatios. (1 Kw=1.36 CV).

Adapta el programa a estos cambios.

c) Modifica la clase Motorizado para hacer getPotenciaReal no abstracta, retornando la potencia en CV.

Crea una referencia a Motorizado y haz que apunte a un objeto Automóvil. ¿Puedes mostrar sus número de puertas? ¿Cómo lo has hecho para no obtener error de compilación? Llama a getPotenciaReal para esa referencia. ¿En qué unidades muestra el valor de potencia? ¿Porqué? ¿Cómo se llama a la relación entre los métodos getPotenciaReal de Motorizado y Automóvil? ¿Podrías hacer un ejemplo de overloading entre ambas clases?

5.  Realiza un programa que:

 - define la clase Pajaro con un atributo longitudPico, un constructor con un parámetro, setter, getter, método pia() (que muestre en pantalla "PIO") y método abstracto vuela().

 - define la clase Colibri que herede de la anterior e incluya un atributo color de tipo String, con su correspondiente setter y getter, un constructor con 2 parámetros desde el cual se llame al constructor de su clase base, un constructor de copia, y redefina o sobreescriba (overriding) el método pia (un colibrí no hace "PIO", sino "PIO, PIO", y lo hace llamando dos veces al método pia() de su clase base). Además, un colibrí vuela a 30 Km/h (lo mostrará por pantalla al volar), y empolla sus huevos (muestra por pantalla que está empollando sus huevos).

 - crea un objeto Colibri y hazlo piar, volar y empollar.

 - crea un segundo objeto Colibri, copia del anterior.

 ¿Qué cambiaría si los objetos colibrí se crearan con una referencia a Pajaro? Explícalo en el propio código fuente del programa.


6. Realiza un programa en el que:

 - crees un paquete "motores" e incluyas en él:
 - una interfaz Potencia con un método gira()
 - una clase Motor que implemente la interfaz anterior y, al menos, con un atributo velocidad, un constructor con un parámetro, setter, getter, y método abstracto acelera().
 - define la clase MotorElectrico que herede de la anterior e incluya un atributo Voltaje de tipo String (por ejemplo 220CA o 12CC, para 220 Voltios de corriente alterna o 12 Voltios de corriente continua), con su correspondiente setter y getter, un constructor con 2 parámetros desde el cual se llame al constructor de su clase base, un constructor de copia, y redefina o sobreescriba (overriding) el método gira (un MotorElectrico  gira a 3000 rpm, mientras el resto de motores lo hace a 1500). Además, el MotorElectrico incrementa su velocidad acelerando, con el método acelera, a 10 rpm más (revoluciones por minuto, incrementará su velocidad en esta cantidad al acelerar).
 
 	Realiza un programa que utilice las clases del paquete anterior:
 - crea un objeto MotorElectrico y hazlo girar y acelerar dos veces.
 - crea un segundo MotorElectrico, copia superficial del anterior.
 - crea un nuevo objeto MotorElectrico con referencia de tipo Motor. Hazlo acelerar.

7. Realiza un ejercicio en el que se defina una interfaz Transporte con un método consumoViaje al cual se le pase los kilómetros realizados y devuelva el importe de su coste en euros. Define una clase Vehículo (potencia, consumo) que implemente la interfaz anterior, y dos clases Coche (puertas) y Moto (tipo) que hereden de la anterior. En los constructores de las clases derivadas debes hacer uso de super para llamar al constructor de la clase base pasándole los parámetros que éste requiera.
Realiza un programa que pruebe las clases anteriores.

8. Define una clase jugador (con atributo nombre) que defina un método final muestraNombre, un método abstracto muestraJugador. Haz una segunda clase Futbolista que herede de la anterior con el atributo goles.

- Realiza un programa que intente instanciar algún objeto de cada una de las clases anteriores.

- Intenta redefinir o sobreescribir el método muestraNombre en la clase Futbolista. ¿Es posible? ¿Porqué?

- Crea un segundo objeto Futbolista (f2 por ejemplo, suponiendo que el primero fuera f1), que sea final. Haz que la referencia de este segundo objeto apunte al primero (f2=f1). ¿Qué sucede? ¿Porqué?
