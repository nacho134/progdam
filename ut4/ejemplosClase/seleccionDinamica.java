// Ejemplo de sobreescritura y selección dinámica (en tiempo de ejecución) del método a llamar, según sea el tipo de objeto apuntado

class A {
	void hola() {
		System.out.println("Estoy en A");
	}

}
class B extends A {
	void hola() {
		System.out.println("Estoy en B");
	}
}
class C extends A {
	void hola() {
		System.out.println("Estoy en C");
	}
}
class seleccionDinamica {
	public static void main (String args []) {
		A a = new A();
		B b = new B();
		C c = new C();
		A r; // Obtención de una referencia a un objeto A
		r = a;
		r.hola();
		r = b;
		r.hola();
		r = c;
		r.hola();
	}
}
