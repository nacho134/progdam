// Nuestro primer ejemplo de interfaz
{
	public double area();
}

public class rectangulo implements figura
{
	static
	{
		System.out.println("Inicializando clase rectangulo");
	}
	public static int contR;
	// atributos o propiedades (por defecto, private)
	private double ancho;
	private double alto;
	// Constructor por defecto
	public rectangulo() { ancho=alto=1; contR++;}
	// Constructor general
	public rectangulo(double an,double al)
	{
		ancho=an;
		alto=al;
		contR++;
	}
	// constructor para hacer un cuadrado
	public rectangulo(double lado)
	{
		ancho=alto=lado;contR++;
	}
	// constructor DE COPIA
	public rectangulo(rectangulo r)
	{
		ancho = r.ancho;
		alto = r.alto;
		contR++;
	}
	// métodos o funciones: definen el comportamiento (por defecto, public)
	// métodos SETTERS
	public void setAncho(double an) { ancho = an; }
	public void setAlto(double al) { alto = al; }
	// métodos GETTERS
	public double getAncho() { return ancho; }
	public double getAlto() { return alto; }
/*	public double area()
	{
		return ancho * alto;
	}*/
	public double perimetro()
	{
		return 2*(ancho + alto);
	}
	// método que me permita cambiar ancho y alto
/*	public void cambiaValores(double an,double al)
	{
		ancho = an;
		alto = al;
	}*/
	// sobreescritura del método toString
	public String toString()
	{
		return "ancho: " + ancho + ", alto: " + alto;
	}
}
