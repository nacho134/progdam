class persona {
	protected String nombre;
	public void setNombre(String nom){
		nombre = nom;
	}
	public String getNombre(){
		return nombre;
	}
}
class empleado extends persona{
	protected int sueldoBase;
	public int getSueldo(){ return sueldoBase; }
	public void setSueldoBase(int s){ sueldoBase = s; }
}

class encargado extends empleado{
	private String puesto;
	public void setPuesto(String s) { puesto=s; }
	public String getPuesto() { return puesto; }
	public int getSueldo(){ 
		Double d = new Double(sueldoBase*1.1);
		return d.intValue(); 
	}
}

public class polimorfismo
{
	public static void main(String[] args) {
		persona p1;
		p1=new empleado();
		// aquí no hay error de compilación porque el método está definido en la clase base (persona) que es como se ha definido la referencia
		p1.setNombre("Isaac Sánchez");
		// Da error de compilación porque no me permite acceder con una referencia de la clase base a un método de la clase derivada. Se soluciona moldeando la referencia, que era a la clase base, a la clase derivada
		((empleado)p1).setSueldoBase(100);

		empleado e1;
		e1=new encargado();
		// no da error
		e1.setSueldoBase(500);
		// sí da error, pero se corrige con el moldeado
		((encargado)e1).setPuesto("Jefe almacén");
		System.out.println(e1.getSueldo());
			
		System.exit(0);
	}
}
