/*6. Realiza un programa en el que:
 - crees un paquete "motores" e incluyas en él:
 - una interfaz Potencia con un método gira()
 - una clase Motor que implemente la interfaz anterior y, al menos, con un atributo velocidad, un constructor con un parámetro, setter, getter, y método abstracto acelera().
 - define la clase MotorElectrico que herede de la anterior e incluya un atributo Voltaje de tipo String
  (por ejemplo 220CA o 12CC, para 220 Voltios de corriente alterna o 12 Voltios de corriente continua), 
  con su correspondiente setter y getter, un constructor con 2 parámetros desde el cual se llame al constructor de su clase base, un constructor de copia, y redefina o sobreescriba (overriding) el método gira (un MotorElectrico  gira  a 3000 rpm, mientras el resto de motores lo hace a 1500). Además, el MotorElectrico incrementa su velocidad 
  acelerando, con el método acelera, a 10 rpm más (revoluciones por minuto, incrementará su velocidad en esta cantidad al acelerar).
 */
package motores;
import java.util.Scanner;

public abstract class Motor implements Potencia{
	protected double velocidad;
	public Motor(double velocidad) { this.velocidad = velocidad; }
	
	public void setVelocidad(double velocidad) { this.velocidad = velocidad; }
	public double getVelocidad() { return velocidad; }
	
	public void gira() {
		//return "\nEste motor gira a 1500 rpm.";
		velocidad = 1500;
	}
	public abstract void acelera();// { velocidad += 50; }
	public String toString() {
		return "\nVelocidad actual: " + velocidad;
	}
}

class MotorElectrico extends Motor{
	
	private String voltaje;
	public MotorElectrico(String volt) {
		super(3000);
		voltaje=volt;
	}
	public MotorElectrico(double velocidad,String volt) {
		super(velocidad);
		voltaje=volt;
	}
	public void setVoltaje(String v) { voltaje = v; }
/*	public void setVoltaje() {
		String vol; Scanner ent = new Scanner(System.in);
		System.out.println("Inicializando MotorElectrico...\n¿Corriente alterna o continua? CA / CC");
		vol = ent.nextLine();
		vol = vol.toUpperCase();
		if(vol.equals("CA")) {
			voltaje = "220 CA";
		} else if(vol.equals("CC")) {
			voltaje = "12 CC";
		} else
			voltaje = "Sin definir";
	}*/
	public String getVoltaje() { return voltaje; }
	
	public void gira() { /*return "\nEste motor gira a 3000 rpm.";*/ velocidad=3000; }
	
	public void acelera() { velocidad += 10; }
	
	public String toString() { return super.toString() + "\nVoltaje: " + voltaje; }
}

