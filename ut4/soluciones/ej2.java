/*Realiza un programa en Java que haga lo siguiente:

- lea un valor numérico desde teclado
- cree un objeto Double para el valor introducido con el constructor que requiere como parámetro un String
- obtenga el valor double (tipo básico) correspondiente
- cree un segundo objeto Double para el valor introducido con el constructor que requiere como parámetro el double obtenido anteriormente
- obtenga a partir del Double primero el valor entero (int) más próximo (redondeo) y el valor truncado
- obtenga el valor hexadecimal, octal y binario correspondiente al entero truncado.*/

public class ej2
{
	public static void main(String[] args)
	{
		System.out.println("Introduce un número:");
		double n = Double.parseDouble(System.console().readLine());
		
		//crear objeto Double a partir del valor introducido
		//Double d1 = new Double(n);
		Double d1 = Double.valueOf(n);

		//obtener valor básico correspondiente
		double n1 = d1.doubleValue();
	
		System.out.println(n1);

		//Double d2 = new Double(n2);
		Double d2 = Double.valueOf(n1);

		//redondeo (TAMBIÉN PODRÍA HABERSE HECHO CON MATH.ROUND()
		if ((n1 - (int)n1)<0.5) 
			System.out.println("Entero más próximo: " + (int)n1);
		else
			System.out.println("Entero más próximo: " + (int)(n1+1));

		//valor truncado (sin decimales)
		int num = d1.intValue();

		System.out.println("Valor truncado: " + num);

		//crear objeto Integer a partir del valor truncado y convertirlo a hexadecimal, octal y binario
		//Integer int1 = new Integer(num);
		Integer int1 = Integer.valueOf(num);
		String hex = int1.toHexString(num);
		System.out.println("En hexadecimal: " + hex);
		String oct = int1.toOctalString(num);
		System.out.println("En octal: " + oct);
		String bin = int1.toBinaryString(num);
		System.out.println("En binario: " + bin);	
	}
}
