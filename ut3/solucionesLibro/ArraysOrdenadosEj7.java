//Ej 7 página 227 libro. Realiza un método que tome como parámetros de entrada dos arrays de enteros y devuelva como salida un único array con los elementos de los anteriores arrays ordenados de forma ascendente.
//package arrays_ejercicios;

import java.util.Scanner;

public class ArraysOrdenadosEj7 {
		
	public static void main(String[] args) {
		int size1 = (int)(1 + 10*Math.random()); //hacemos que el tamaño de cada array sea aleatorio de 1 a 10
		int array1[] = new int[size1]; //declaramos el primer array
		int size2 = (int)(1 + 10*Math.random());
		int array2[] = new int[size2]; //declaramos el segundo array
		int completeArray[] = null;
		ArrayInput(array1);
		ArrayInput(array2); 
		
		System.out.println("El primer array está formado por:");
		Print(array1);
		
		System.out.println("El segundo array está formado por:");
		Print(array2);
        	
       	completeArray = Merge(array1,array2);
        	System.out.println("El array completo es:");
 		Print(completeArray);
 		
		//Order(completeArray);
		System.out.println("El array completo y ordenado en sentido ascendente es:");
		Print(completeArray);
 	    
		//kb.close(); //cerramos Scanner
		System.exit(0);
	}
	
	public static void ArrayInput(int[] array) { //método que recoge los valores por teclado del array
		Scanner kb = new Scanner(System.in);
		System.out.println("Introduce " + array.length + " valores para guardar en el array: ");
		
		for (int i = 0; i < array.length ; i++)  //rellenamos el array con los datos introducidos por el usuario
			array[i] = kb.nextInt(); //(int)(1 + 50 * Math.random()); para poner los valores aleatorios de 1 a 50	
	}
	
	public static void Print(int array[]) { //método para imprimir los array
		
		for (int i = 0; i < array.length; i++) {
			System.out.print("[" + array[i] + "]" + "\t");
		}
		System.out.println("" + "\n");
	}
	
	public static int[] Merge(int[] array1, int[] array2) { //método para unir ambos array en un único array
		final int TAM = array1.length + array2.length;
		int completeArray[] = new int[TAM]; //new int[length]; //creamos un array con la longitud de ambos unidos
		int position = 0;
		
			for (int i = 0; i < array1.length; i++) { //recorremos el primer array y vamos asignando sus valores al array total.
				completeArray[position] = array1[i];
				position++;	
			}	
			
			for (int j = 0 ; j < array2.length; j++) { //recorremos el segundo array para completar el array total empezando por la posición en la que nos quedamos tras meter los valores del primer array.
				completeArray[position] = array2[j];
				position++;
			}	
		Order(completeArray);
		return completeArray;
	}
	
	public static void Order(int[] completeArray) { //método para ordenar el array en sentido ascendente con el método de la burbuja
		boolean ordered = false;
		for (int i = completeArray.length -2; i >= 0 && !ordered; i--) {
			ordered = true;
			for (int z = 0; z <= i; z++) {
				int a = completeArray[z];
				int b = completeArray[z + 1];
				if (a > b) {
					ordered = false;
					completeArray[z] = b;
					completeArray[z + 1] = a;
				}
			}
		}
	}
}

