// Lee el enunciado de este ejercicio. Descomponlo mediante un programa en frases separadas por el punto. Muestra  cada frase individual. Indica de cuántas frases y palabras se compone
 
import java.util.*;
public class tokenizer
{
    public static void main(String args[])
    {
        // declaracion variables
        String s = "Lee el enunciado de este ejercicio. Descomponlo mediante un programa en frases separadas por el punto. Muestra  cada frase individual. Indica de cuántas frases y palabras se  compone";
        StringTokenizer st = new StringTokenizer(s,".");
        //StringTokenizer st2;
        //int cont=0;

        //imprime el numero de frases y cada una de ellas
        System.out.println("Hay "+ st.countTokens() +  " frases en el enunciado que son las siguientes:\n");
        while (st.hasMoreTokens())
            System.out.println(st.nextToken().trim());
        System.out.println();

/*        //imprime numero de palabras de cada frase
        st = new StringTokenizer("Lee el enunciado de este ejercicio. Descomponlo mediante un programa en frases separadas por el punto. Muestra  cada frase individual. Indica de cuántas frases y palabras se  compone",".");
        while (st.hasMoreTokens())
         {
            cont++;
            st2 = new StringTokenizer(st.nextToken()," ");
            System.out.print("La frase numero " + cont + " tiene " + st2.countTokens() + " palabras\n");
         }*/

        //imprime numero de palabras total del enunciado
        st = new StringTokenizer(s);
        System.out.println("\nHay un total de "+ st.countTokens() +  " palabras en el enunciado\n");
    }
}
