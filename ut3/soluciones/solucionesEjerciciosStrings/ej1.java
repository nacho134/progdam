/* 1. Hacer un programa que ingrese un String y determine el número de mayúsculas y el número de minúsculas. */ 
class ej1
{
    public static void main(String args[])
    {
        if (args.length < 1)
        {
            System.out.println("Modo de uso: java 1 \"String\"");
        }
        else
        {
            char c;
            int may = 0;
            int min = 0;
            for ( int i = 0 ; i < args[0].length() ; i++)
            {
                c = args[0].charAt(i);
		// si c es mayúscula
                if ( c >= 65 && c <= 90)
                    may++;
                else
			// si es minúscula
                    if( c >= 97 && c <= 122)
                        min++;
            }
            System.out.println("Mayúsculas: " + may + "\nMinúsculas: " + min);
        }
    }
}
