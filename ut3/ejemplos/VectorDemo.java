
/* IMPORTANTE:

	La principal diferencia y ventaja de los objetos Vector frente a los ArrayList es que los primeros son sincronizados y los segundos no, lo que significa que todos los métodos que modifican un Vector, por ejemplo add () o remove (), evitan problemas en la programación multihilos. 

Lee más en: http://javarevisited.blogspot.com/2011/09/difference-vector-vs-arraylist-in-java.html#ixzz2Lu4kLvSt */

import java.util.*;

public class VectorDemo
{
  public static void main(String[] args)
  {
	  Vector<Object> vector = new Vector<Object>();
	  
	  int primitiveType = 10;
	  Integer wrapperType = new Integer(20);
	  String str = "tapan joshi";
	  
	  vector.add(primitiveType);
	  vector.add(wrapperType);
	  vector.add(str);
	  vector.add(2, new Integer(30));
	  System.out.println("the elements of vector: " + vector);
	  System.out.println("The size of vector are: " + vector.size());
	  System.out.println("The elements at position 2 is: " 
	  + vector.elementAt(2));
	  System.out.println("The first element of vector is: " 
	  + vector.firstElement());
	  System.out.println("The last element of vector is: " 
	  + vector.lastElement());
	  vector.removeElementAt(2);
	  
	  Enumeration e=vector.elements();
	  System.out.println("The elements of vector: " + vector);
	  while (e.hasMoreElements())
	  	  System.out.println("The elements are: " + e.nextElement()); 
  }
}
