// ejemplo MODULAR de array de tipo básico en el que se retorna un array */

import java.util.Scanner;

public class arrayBasicoModular2
{
	private final static int TAM = 5;
	
	public static void main(String args[])
	{
		// declaro el array de 5 valores numéricos double
		double nums[] = new double[TAM];
		double nums2[] = null;
		// guardo en él los primeros múltiplos de 3
		System.out.println("Introduce " + TAM + " números:");
		nums2 = cargaValores(nums);
		// muestro el contenido del array en pantalla
		muestraValores(nums);
		muestraValores(nums2);
	}
	/* Esta función no es void, retorna el array cargado */
	public static double[] cargaValores(double n[])
	{
		Scanner ent = new Scanner(System.in);
		for (int i = 0 ; i < TAM ; i++)
			n[i] = ent.nextDouble();
		return n;
	}
	
	public static void muestraValores(double n[])
	{
		for (int i = 0 ; i < TAM ; i++)
			System.out.print(n[i] + "\t");
		System.out.println("");
	}

}
