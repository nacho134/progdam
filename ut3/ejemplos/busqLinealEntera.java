// Ejemplo de búsqueda lineal ENTERA (retorna la posición donde lo encuentra)

import java.util.Scanner;

public class busqLinealEntera
{
	public static void main(String args[])
	{
		final int TAM = 5;
		Scanner ent = new Scanner(System.in);
		// declaro el array de 5 valores numéricos double
		double nums[] = new double[TAM];
		// leo valores desde teclado
		for (int i = 0 ; i < TAM ; i++)
			nums[i] = ent.nextDouble();
		// muestro el contenido del array en pantalla
		for (int i = 0 ; i < TAM ; i++)
			System.out.print(nums[i] + "\t");
		System.out.println("¿Qué valor quieres buscar en el array?");
		double n = ent.nextDouble();
		int pos = busquedaLinealEntera(n,nums);
		if ( pos >= 0)
			System.out.println("SI se encuentra en el array, está en la posición " + (pos + 1));
		else
			System.out.println("NO se encuentra en el array");
	}
	
	public static int busquedaLinealEntera(double vb, double array[])
	{
		int i;
		for(i = 0 ; (array[i]!= vb) && (i < array.length) ; i++);
		/* o también:
			for (int i = 0 ; i < array.length ; i++)
				if (array[i] == vb) return true; */
		if (array[i] == vb)	// he salido del bucle porque lo he encontrado
			return i;
		else	// he salido del bucle porque he llegado al final
			return -1;
	}
}
