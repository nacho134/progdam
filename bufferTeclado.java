// Ejemplo del problema del buffer del teclado

import java.util.Scanner;

public class bufferTeclado
{
public static void main(String[] args) 
	{
		double n; String s="";
		
		Scanner ent = new Scanner(System.in);
		System.out.println("Introduce un número:");
		n = ent.nextDouble();
		// para solucionar el problema, aquí vaciamos el buffer
		ent.nextLine();
		System.out.println("Introduce un texto:");
		s = ent.nextLine();
		System.out.println("El número es " + n + ", el texto es " + s);
		
		
		System.exit(0);
	}
}
