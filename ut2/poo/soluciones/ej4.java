// 4. Modifica la clase Disco para cambiar el atributo nombre o artista (previamente de tipo String) por un atributo de tipo Musico (clase definida para el ejercicio anterior). Al mostrar los discos en el programa, has de utilizar el método muestraMusico().

class musico
{
	// atributos
	private String nom;
	private String instr;
	// constructores
	public musico() { nom = "Bob Dylan"; instr = "guitarra";}
	public musico(String n, String i) { nom = n; instr = i;}
	public musico(musico m) { nom = m.nom; instr = m.instr; }
	// setters y getters
	public void setNom(String n) { nom = n; }
	public void setInstr(String i) { instr = i; }
	public String getNom() { return nom; }
	public String getInstr() { return instr; }
	// resto de métodos
	public void muestraMusico() 
	{
		System.out.println("Nombre: " + nom + ",\tinstrumento: " + instr);
	}
		
}

class disco
 {
 	private String titulo;
 	private musico grupo=null;
 	private double precio=15;
 	// métodos: constructores, setters, getters y otros
 	public disco() { titulo = "Street Legal"; grupo = new musico();  }
 	public disco(String t, musico g) { titulo = t; grupo = g; }
 	public disco(String t, musico g, precio p) { titulo = t; grupo = g; precio=p;}
 	public disco(String t, String n, String i) { titulo = t; grupo = new musico(n,i); }
 	public void setTitulo(String t) { titulo = t; }
 	public void setGrupo(musico g) { grupo = g; }
 	public String getTitulo() { return titulo; }
 	public musico getGrupo() { return grupo; }
 	public void muestraDisco()
 	{
 		System.out.println("Título: " + titulo + ", grupo: " + grupo.getNom() + ", instrumento: " + grupo.getInstr() + ", precio: " + precio);
 	}
 	public void oferta()
 	{
 		precio *= 0.8;		// precio = precio*0.8;
 	}
 		
 }
 
 public class ej4
 {
 	public static void main(String[] args) {
 	musico m1 = new musico();
 	disco d1 = new disco();
 	musico m2 = new musico("Pink Floyd","banda");
 	disco d2 = new disco("Whish you were here",m2);
 	d1.muestraDisco();
 	d2.muestraDisco();
 	// cambiamos el intrumento del disco d2
 	d2.getGrupo().setInstr("sintetizadores");
 	d2.muestraDisco();
 }
 
 }
