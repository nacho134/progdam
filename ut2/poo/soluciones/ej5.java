/*5. Define la clase Dado con:

	- un atributo "valor" (que variará entre 1 y 6)
	- constructor con valor por defecto
	- constructor general con valor indicado como parámetro
	- constructor general con valor al azar
	- setter y getter (setValor y getValor)
	- un método "tirada" que cambia el valor del dado al azar
	- otro método "muestra" que dibuje el dado en pantalla con asteriscos
		
Realiza a continuación un programa que ponga a prueba todo lo anterior. Habrás de crear al menos 2 objetos Dado, cada uno con un constructor diferente.*/
import java.util.Scanner;

class dado
{
	//ATRIBUTO
	private int valor;
	//BUILDERS
	//public dado() {valor=1;}
	public dado()
	{
		valor = (int)(1+6*Math.random());
	}
	public dado(int v) 
	{
		if (v < 0)
			v = - v;
		// ahora v ya es positivo
		if ((v % 6) == 0)	// es múltiplo de 6
			valor = 6;
		else	// no es múltiplo de 6
			valor = v % 6;
	}
	//SETTERS Y GETTERS
	public void setValor(int v)
	{
		if (v < 0)
			v = - v;
		if ((v % 6) == 0)	// es múltiplo de 6
			valor = 6;
		else	// no es múltiplo de 6
			valor = v%6;
	}
	public int getValor(){return valor;}
	//RESTO MÉTODOS
	public void tirada()
	{
		valor = (int)(1+6*Math.random());
	}

	public void muestra()
	{
		for (int f=1; f<=3; f++) 
		{
			for (int c=1;c<=3 ;c++ )
			{
				if((c==2) && (f==2))
					System.out.print(valor+" ");
				else
					System.out.print("* ");
			}
			System.out.println("");
		}
	}
	public void show()
	{
		for(int i=1; i <= valor ; i++)
			System.out.print('*');
	}
}

public class ej5
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		dado d1 = new dado();
		//sin acabar estoy actualmente en proceso de pruebas
		d1.muestra();
		d1.tirada();
		d1.muestra();
		d1.tirada();
		d1.muestra();
		dado d2 = new dado(8);
		//sin acabar estoy actualmente en proceso de pruebas
		d2.muestra();
		d2.tirada();
		d2.muestra();
		d2.tirada();
		d1.show();
		d1.setValor(7);
		d1.show();
	}
}
