/* 2. Crea una clase Musico que incluya 2 atributos (nombre del músico e instrumento, ambos de tipo String). La clase incluirá también, al menos:

 - un constructor que inicialice el músico con un nombre y un instrumento inicial.
 - constructor de copia.
 - un método muestraMusico (o toString()) que muestre los datos del músico por pantalla.
 - los correspondientes métodos "setters" y "getters" (setNombre,setInstr,getNombre,getInstr) que cambien el instrumento o el nombre (métodos setXxx), o devuelvan esos mismos valores (métodos getXxx).

	Realiza un programa que, utilizando la clase Musico(*), defina al menos dos objetos de este tipo, muestre sus datos, modifique algún dato de cada uno de ellos y acabe mostrando sus nombres, instrumentos y el total de músicos.

(*) utiliza la clase Musico del ejercicio anterior, añadiéndole un tercer atributo: número de músicos (este atributo se incrementará en una unidad para cada músico nuevo que se cree). */

import java.util.Scanner;

class musico
{
	// atributos
	private String nom;
	private String instr;
	// constructores
	public musico() { nom = "Bob Dylan"; instr = "guitarra";}
	public musico(String n, String i) { nom = n; instr = i;}
	public musico(musico m) { nom = m.nom; instr = m.instr; }
	// setters y getters
	public void setNom(String n) { nom = n; }
	public void setInstr(String i) { instr = i; }
	public String getNom() { return nom; }
	public String getInstr() { return instr; }
	// resto de métodos
	public void muestraMusico()
	{
		System.out.println("Nombre: " + nom + ",\tinstrumento: " + instr);
	}
}

public class ej2
{
	public static void main(String[] args) {
	musico m1 = new musico();	// Bob Dylan
	musico m2 = new musico("Bill Evans","piano");	// constructor general
	musico m3 = new musico(m1);
	// muestro cada musico
	m1.muestraMusico();
	m2.muestraMusico();
	m3.muestraMusico();
	m3.setNom("Paco de Lucía");
	m2.setInstr("saxofón");
	System.out.println("El segundo músico es " + m2.getNom() + " que toca el " + m2.getInstr());
	m1.muestraMusico();
	m2.muestraMusico();
	m3.muestraMusico();
	
		System.exit(0);
	}
}
