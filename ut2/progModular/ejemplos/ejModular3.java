/* Ejemplo MODULAR de función de tipo void SIN PARÁMETROS

Programa que muestre en pantalla UN asterisco (*) */

public class ejModular3
{
	public static void main(String[] args)
	{
		int n;
		
		//System.out.println("Introduce un entero positivo");
		//n = Integer.parseInt(System.console().readLine());
		// LLAMADA a la función
		imprimeAsterisco();
		System.exit(0);
	}
	
	// DEFINICIÓN de la función
	public static void imprimeAsterisco()
	{
		//int i;
		
		//for (i=1 ; i <= num; i++)
			System.out.println('*');
	}
}
