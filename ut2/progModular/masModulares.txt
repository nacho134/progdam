1. Realizar una funci�n que reciba una variable de tipo caracter y si es una letra min�scula la devuelva convertida en may�sculas. 

2. Realizar una funci�n que reciba un n�mero real (float) y devuelva el n�mero entero m�s pr�ximo.

3. Realizar una funci�n que reciba una cantidad de horas, minutos y segundos y la pase a segundos.

4. Realizar un programa que pida un n�mero al usuario y d� la lista de los factoriales de los n�meros desde el pedido hasta el 1, indicando cu�les de los factoriales hallados tienen alg�n d�gito igual a 2.
	Se utilizar� una funci�n para el c�lculo del factorial de cada n�mero y otra para saber si el factorial calculado tiene o no alg�n d�gito igual a 2.
	Ejemplo para el 5:

	5: factorial = 120	SI tiene 2
	4: factorial = 24	SI tiene 2
	3: factorial = 6	NO tiene 2
	2: factorial = 2	SI tiene 2
	1: factorial = 1	NO tiene 2

5. Indicar cu�l es el resultado de ejecutar el siguiente programa si se introducen como entrada los n�meros 4,3,2,1 y 0:

/* define T� el objeto Scanner necesario */
public class ej5
{
	public static void main(String args[])
	{
		procesar_numeros();
	}

	public static void procesar_numeros()
	{
		int n;

		System.out.println("Dar un n�mero:");
		n = ent.nextInt();
		if (n != 0)
		{
			procesar_numeros();
			System.out.println(n);
		}
	}
}

6. Modificar el programa anterior para que escriba los n�meros pares en el mismo orden en el que se leen. Es decir, que si se dan los n�meros 6,4,3,2,1,0 escriba en pantalla 6,4,2.

7. Realizar un programa que calcule los n�meros primos entre 1 y 1000, utilizando una funci�n que indique si un n�mero recibido como par�metro es o no primo.

8. Realizar un programa, con un subprograma recursivo, que dado un n�mero entero positivo, d� la suma de todos los n�meros pares positivos menores que �l.

9. Realizar un programa que escriba en pantalla n l�neas del tri�ngulo de PASCAL utilizando un subprograma recursivo.
	En el tri�ngulo de PASCAL los n�meros de los lados valen 1 y los del centro se calculan sumando los dos que tiene inmediatamente encima:

					1
				1		1
			1		2		1
		1		3		3		1
	1		4		6		4		1
..............................................................................

	Se aconseja utilizar una funci�n recursiva que calcule el valor de cada t�rmino dada su posici�n (fila y columna). Por ejemplo, que calcule que el elemento de la fila 5 y columna 3 es un 6.

10. Hacer un programa en Java que imprima 100 asteriscos en pantalla utilizando una funci�n main recursiva.

11. Realiza un programa, con una funci�n main RECURSIVA, que muestre todos los enteros desde 1 hasta 100.

12. Los n�meros de Fibonacci son los miembros de una secuencia en la que cada numero es igual a la suma de los dos anteriores. En otras palabras, Fi = Fi-1 + Fi-2, donde Fi es el i-�simo n� de Fibonacci, y siendo F1=F2=1. Por tanto:
	F5 = F4 + F3 = 3 + 2 = 5, y as� sucesivamente.
	Escribir un programa que determine los n primeros n�meros de Fibonacci, siendo n un valor introducido por teclado.

El programa deber� realizarse con c�lculo recursivo.
