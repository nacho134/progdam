// 10. Hacer un programa en Java que imprima 100 asteriscos en pantalla utilizando una función main recursiva.

public class ej10
{
	private static int cont=0;
	public static void main(String[] args)
	{
		System.out.print('*');
		cont++;
		if (cont < 100)
			main(args);
		System.exit(0);
	}
}
