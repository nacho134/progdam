// 11. Realiza un programa, con una función main RECURSIVA, que muestre todos los enteros desde 1 hasta 100.

public class ej11
{
	private static int cont=1;
	public static void main(String[] args)
	{
		System.out.println(cont);
		cont++;
		if (cont <= 100)
			main(args);
		System.exit(0);
	}
}
