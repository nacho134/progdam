// Programa equivalente al comando cat: admitirá como parámetro el nombre (sólo nombre o ruta parcial, o completa) de un fichero de texto y volcará a pantalla su contenido

import java.io.*;

public class ejCat
{
	public static void main(String[] args) //throws FileNotFoundException,IOException 
	{
		if (args.length > 0)
		{
			File f = new File(args[0]);
			if (f.exists())
			{
				try
				{
					FileInputStream fis = new FileInputStream(f);
					int c= fis.read();
					while(c != -1)
					{
						System.out.print((char)c);
						c = fis.read();
					}
					// no he de olvidarme de cerrar con close()
					fis.close();
				}
				/*catch (FileNotFoundException e)
				{
					System.err.println("El fichero no existe");
				}*/
				catch (IOException e)
				{
					System.err.println(e.getMessage());
				}
			}
			else
				System.out.println("Ese fichero no existe");
		}
		else
			System.out.println("Forma de uso: ejCat /ruta/al/fichero");
	}
}
