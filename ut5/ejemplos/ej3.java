import java.io.*;

public class ej3 {
  public static void main(String args[]) throws IOException {
    PrintWriter pantalla = new PrintWriter(System.out);
    //char[] array = { 'M', 'o', 'r', 'e', 'n', 'o' };
    //char array[]={"Moreno"};
    char array[]="Moreno";
    String str = new String("Juan Carlos");

    pantalla.write(str);
    pantalla.print(" ");
    pantalla.write(array, 0, 6);

    pantalla.println("");

    pantalla.flush();
  }
}

// Comprueba si el array de caracteres podría haberse construido con las asignaciones de las líneas 7 u 8. ¿Porqué?
