import java.io.*;
public class ej5
{
	public static void main(String[] args)
	{
		FileOutputStream f=null;
		boolean ap=true;
		String s = "Un sitio de la mancha de cuyo nombre no quiero acordarme...";
		char c;
		try{
			f=new FileOutputStream("datos.txt",ap);
			
			for (int i=0; i<s.length();i++){
				c=s.charAt(i);
				f.write((byte)c);
			}
		}catch(IOException e){
			e.printStackTrace();
		}finally{
			try{
				f.close();
			}catch(IOException e){
				e.printStackTrace();
			}
		}
	}
}
