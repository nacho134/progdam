/* 3. Programa que añada una línea de texto a un fichero.
 Ambos se pasarán como parámetros en la ejecución.*/

import java.io.*;

public class ej3
{
	public static void main(String[] args) {
		if (args.length > 1)
		{
			try (FileWriter fw = new FileWriter(args[0],true);)
			{
				fw.write(args[1] + "\n");
			}
			catch(IOException e)
			{
				System.err.println(e.getMessage());
			}
		}
		else // cuando no se pasen al menos dos parámetros
			System.out.println("Forma de uso: java ej3 fichero \"mensaje a añadir\"");
	}
}// Fin del programa
