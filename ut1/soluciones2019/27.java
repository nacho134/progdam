// Modificar el anterior algoritmo para que pida una lista de enteros acabada con un 0  e indique, para cada uno de ellos, si es primo o no. Además, al final ha de indicar cuántos primos se han introducido.

public class p18
{
    public static void main(String args[])
    {
        int num,cont=0;
        boolean primo;
        
	System.out.println("Introduzca un número entero:");
	num = Integer.parseInt(System.console().readLine());
	while (num != 0)
	{
	   primo = true;
	    for (int a = 2; a <= num/2 ; a++)
	        if ((num % a) == 0)
	        {
	            primo = false;
	            break;
	        }
	     if (primo == true)
		{
		       System.out.println("El número es primo");
			cont++;
		}
	     else
		        System.out.println("El número no es primo");
	     System.out.println("Introduzca un número entero:");
	     num = Integer.parseInt(System.console().readLine());
	}
	System.out.println("Se han introducido " + cont + " primos.");
    }
}
