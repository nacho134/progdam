/*. Los números de Fibonacci son los miembros de una secuencia en la que cada numero es igual a la suma de los dos anteriores. En otras palabras, Fi = Fi-1 + Fi-2, donde Fi es el i-ésimo nº de Fibonacci, y siendo F1=F2=1. Por tanto:
	F5 = F4 + F3 = 3 + 2 = 5, y así sucesivamente.
Escribir un programa que determine los n primeros números de Fibonacci, siendo n un valor introducido por teclado.
*/
public class p19{
	public static void main(String[] args) {
		int n,f1,f2,aux=0;

		System.out.print("Introduce un número mayor que 2: ");
		n = Integer.parseInt(System.console().readLine());
		if ( n > 1)
		{
			f1 = f2 = 1;
			System.out.println(f1 + "\n" + f2);

			for(int i = 2 ; i < n ; i++){
				aux = f1 + f2;
				System.out.println(aux);
				f1 = f2;
				f2 = aux;
			}
		}
		else
			if ( n == 1)
				System.out.println("1");
	}
}
