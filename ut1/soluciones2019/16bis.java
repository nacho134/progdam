/*Programa que indique el máximo y el mínimo de un conjunto de notas introducidas desde teclado
La serie de notas acabará con un valor negativo*/
public class p7bis
{
	public static void main(String[] args) 
	{
		double nota,max=-1,min=11;
		
		do
		{
			System.out.println("introduce un numero entre 0 y 10 (negativo para acabar)");
			nota=Double.parseDouble(System.console().readLine());
			if ((nota > max) && (nota <= 10))
			{
				max = nota;
			}
			if ((nota < min) && (nota >= 0))
			{
				min = nota;
			}
		}
		while (nota >= 0 );
		if (max >= 0)
		{
			System.out.println("el numero mas alto ha sido: " + max );
			System.out.println("el numero mas bajo ha sido: " + min );
		}
	}
}
