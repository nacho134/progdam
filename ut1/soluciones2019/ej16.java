//16. Programa que indique el máximo y el mínimo de un conjunto de notas introducidas desde teclado. La serie de notas acabará con un valor negativo.

import java.util.Scanner;

public class ej16
{
    public static void main(String args[])
    {
    	// x es la nota
		double x , min=11 , max=-1 ;
		
		Scanner ent = new Scanner (System.in);
		System.out.println("Introduzca una nota(la serie acabara con un valor negativo): ");
		x = ent.nextDouble();
		while ( x >= 0)
		{
			if ((x>max) && (x<=10))
		        max=x;
		    if (x<min)
		        min = x ;
		    System.out.println("Introduzca una nota(la serie acabara con un valor negativo): ");
		    x = ent.nextDouble();
		}    
		if (max  >= 0)  
		    System.out.println("La nota mas alta es " + max + " y la mas baja es " + min );
	    else
		    System.out.println("No hay valores");	                    
    }
}

