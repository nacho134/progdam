// 2. Programa que pide 2 valores numéricos y muestra el resultado de su suma y su producto.

public class ej2
{
	public static void main(String args[])
	{
		double n1,n2;

		System.out.println("Introduce dos números:");
		n1 = Double.parseDouble(System.console().readLine());
		n2 = Double.parseDouble(System.console().readLine());
		// Muestro suma y producto
		System.out.println("La suma es " + (n1 + n2) + " y su producto es " + (n1*n2));	
	}
}
