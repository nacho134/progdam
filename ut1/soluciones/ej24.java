/* 24. Algoritmo que lea números desde teclado y finalice cuando se introduzca uno mayor que la suma de los dos anteriores. Escribir en pantalla el numero de valores introducidos
 y los valores que cumplieron la condición de finalización. */
 
import java.util.Scanner;
public class ej24
{
	public static void main(String[] args)
	{
		double num1,num2,num3;	int contador = 3;
		Scanner entrada = new Scanner(System.in);

		System.out.println("Introduce tres números.Cuando el tercero sea mayor que la suma de los dos anteriores, terminará el programa. ");
		num1 = entrada.nextDouble();
		num2 = entrada.nextDouble();
		num3 = entrada.nextDouble();
		while ( num3 <= num1+num2 )	// se repite mientras el último no sea mayor que los dos anteriores
		{
			contador++;
			System.out.println("Introduce otro valor");
			num1=num2;
			num2=num3;
			num3 = entrada.nextDouble();	
		}
		System.out.println("Se han introducido " + contador + " valores, y los que han cumplido la condición son: " + num1 + "," + num2 + " y " + num3);
	}
}
