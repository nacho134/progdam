//Algoritmo que indique si un número introducido por teclado es primo.

import java.util.*;

public class ej26
{
    public static void main (String args[])
    {
        int n, i, tope;
        Scanner ent = new Scanner(System.in);
 
        System.out.println("Introduce un entero mayor que cero para conocer si es primo");
        n = ent.nextInt();
		tope = n/2;
       for (i = 2; i <= tope; i++)
           if (n%i==0)	// si la división es exacta
           {
            System.out.println("\nEl número introducido ("+n+") NO ES PRIMO\n");
            System.exit(0);	// en lugar del break
           }

       System.out.println("\nEl número introducido ("+n+") ES PRIMO\n");
    }
}
