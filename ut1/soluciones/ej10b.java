//10*. Programa que muestra las 10 tablas de multiplicar (para 1,2,3,...,10).

public class ej10b{

    public static void main(String args[])
    {

        int j,i;
        
        //System.out.println();
        System.out.println("\nLista de las tablas de multiplicar del 1 al 10:\n");
        //System.out.println();

        for(j=1; j<=10; j++)
            for(i=1; i<=10; i++)     
            	System.out.println(j + " * " + i + " = " + (i * j) + "  ");
	}
}
